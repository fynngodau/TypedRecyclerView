/*
 * CC0 – No rights reserved
 *
 * To the extent possible under law, Fynn Godau has waived all copyright and related
 * or neighboring rights to TypedRecyclerView. This work is published from Germany.
 */
package godau.fynn.typedrecyclerview

import android.util.Log
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.ViewHolder

/**
 * `<V>`: List value class
 */
abstract class TypedRecyclerViewAdapter<V : Any> @JvmOverloads constructor(content: MutableList<V> = mutableListOf()) :
    SimpleRecyclerViewAdapter<V, ViewHolder>(content) {

    private val handlerSet: MutableList<TypeHandler<*, out V>> = mutableListOf()

    /**
     * Maps type handler classes to type handler instances. Used to handle return values of
     * [getItemHandlerClass] efficiently.
     */
    private val classInstanceMap =
        mutableMapOf<Class<out TypeHandler<*, out V>>, TypeHandler<*, out V>>()

    /**
     * Values in `0…Integer.MAX_VALUE` represent values returned by
     * [getItemHandler], values in `Integer.MIN_VALUE…-1`
     * represent values returned by [getItemHandlerClass].
     */
    override fun getItemViewType(position: Int): Int {
        val item = content[position]
        val handlerClass = getItemHandlerClass(item, position)
        if (handlerClass != null) {
            // Construct a TypeHandler from class returned by getItemHandlerClass(V, int)
            return if (classInstanceMap.containsKey(handlerClass)) {
                -positionInSet(classInstanceMap.keys, handlerClass)!! - 1
            } else try {
                Log.v("TypedRecyclerView", "Constructing " + handlerClass.name)
                val typeHandler = handlerClass.newInstance()
                classInstanceMap[handlerClass] = typeHandler
                -positionInSet(classInstanceMap.keys, handlerClass)!! - 1
            } catch (e: IllegalAccessException) {
                throw UnsupportedOperationException(
                    "The TypeHandler class " + handlerClass.name +
                            " is either not public or does not have public access on its constructor.",
                    e
                )
            } catch (e: InstantiationException) {
                throw UnsupportedOperationException(
                    "The TypeHandler class " + handlerClass.name + "could not " +
                            "be instantiated. It needs to have a nullary constructor (a constructor without parameters) " +
                            "and it must not be abstract.", e
                )
            }
        }
        val typeHandler = getItemHandler(content[position], position)
        if (typeHandler != null) {
            val inserted = handlerSet.add(typeHandler)
            if (inserted) {
                Log.v(
                    "TypedRecyclerView",
                    "First use of handler " + typeHandler.javaClass.simpleName
                )
            }
            return positionInSet(handlerSet, typeHandler)!!
        }
        throw UnsupportedOperationException(
            "You must implement either getItemHandlerClass(V, int) or " +
                    "getItemHandler(V, int)!"
        )
    }

    /**
     * @return A [TypeHandler] which should handle this item at this position
     */
    protected open fun getItemHandler(item: V, position: Int): TypeHandler<*, out V>? {
        return null
    }

    /**
     * Implementing this method is recommended over implementing [getItemHandler],
     * as it provides a cleaner API, but one of the two must be implemented. If this method is
     * implemented, the TypeHandler class needs to have a public empty constructor (which is the
     * default constructor in case of no constructor).
     *
     *
     * If both methods are implemented, this method has priority over the other one. Return null
     * to evaluate the other method.
     *
     * @param item Item the TypeHandler needs to handle
     * @param position Position the item is at
     * @return The class of a [TypeHandler] which should handle this item at this position
     */
    protected open fun getItemHandlerClass(
        item: V,
        position: Int
    ): Class<out TypeHandler<*, out V>>? {
        return null
    }

    private fun getTypeHandler(viewType: Int): TypeHandler<*, *> {
        val handler: TypeHandler<*, out V> = if (viewType < 0) {
            itemAtPosition(classInstanceMap.values, -viewType - 1)!!
        } else {
            itemAtPosition(handlerSet, viewType)!!
        }

        // Copy fields (note that these are all reference types)
        handler.set(context, inflater, content, this, recyclerViews)
        return handler
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return getTypeHandler(viewType).createViewHolder(parent)!!
    }

    override fun onBindViewHolder(holder: ViewHolder, item: V, position: Int) {
        val typeHandler = getTypeHandler(getItemViewType(position))
        // Unchecked call, but it works magic
        typeHandler.internalBindViewHolder(holder, item, position)
    }

    /**
     * @param <C> Type of set and class of object
     * @return Position of `object` in `collection`
    </C> */
    private fun <C> positionInSet(collection: Collection<C>, `object`: C): Int? =
        collection.indexOf(`object`).let { if (it == -1) null else it }

    /**
     * @return Object at `position` of `collection`
     */
    private fun <C> itemAtPosition(collection: Collection<C>, position: Int): C? =
        collection.filterIndexed { index, _ -> index == position }.firstOrNull()
}