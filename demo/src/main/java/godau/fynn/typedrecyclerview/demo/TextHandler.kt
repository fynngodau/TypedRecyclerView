package godau.fynn.typedrecyclerview.demo

import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import godau.fynn.typedrecyclerview.PrimitiveViewHolder
import godau.fynn.typedrecyclerview.TypeHandler

open class TextHandler<A : CharSequence>(private val onClick: View.OnClickListener = View.OnClickListener { }) :
    TypeHandler<PrimitiveViewHolder<TextView>, A>() {
    override fun createViewHolder(parent: ViewGroup): PrimitiveViewHolder<TextView> {
        return PrimitiveViewHolder(
            inflater.inflate(R.layout.row_string, parent, false) as TextView
        )
    }

    override fun bindViewHolder(
        holder: PrimitiveViewHolder<TextView>,
        item: A,
        position: Int
    ) {
        holder.view.apply {
            text = item
            color()
            setOnClickListener(onClick)
        }
    }

    protected open fun TextView.color() {
        setTextColor(Color.BLACK)
    }
}