package godau.fynn.typedrecyclerview

import android.view.View
import androidx.recyclerview.widget.RecyclerView.ViewHolder

/**
 * Holds access to only one single view.
 *
 * `<V>`: Class of the view
 */
data class PrimitiveViewHolder<V : View>(val view: V) : ViewHolder(view)