package godau.fynn.typedrecyclerview

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder

/**
 * Handles a specific view type
 *
 *
 * Subclasses will have access to `context`, `inflater`
 * as well as `content` fields.
 *
 * `<VH>`: View holder class
 *
 * `<V>`: Class of items that are handled (value class)
 */
abstract class TypeHandler<VH : ViewHolder?, V> {
    // All of these are guaranteed to be initialized when createViewHolder or bindViewHolder are called.
    protected lateinit var context: Context
    protected lateinit var inflater: LayoutInflater
    protected lateinit var content: List<V>
    protected lateinit var adapter: TypedRecyclerViewAdapter<*>
    protected lateinit var recyclerViews: List<RecyclerView>
    fun set(
        context: Context,
        inflater: LayoutInflater,
        content: List<Any>,
        adapter: TypedRecyclerViewAdapter<*>,
        recyclerViews: List<RecyclerView>
    ) {
        this.context = context
        this.inflater = inflater
        this.content = content.map { it as V } // unchecked magic
        this.adapter = adapter
        this.recyclerViews = recyclerViews
    }

    abstract fun createViewHolder(parent: ViewGroup): VH
    abstract fun bindViewHolder(holder: VH, item: V, position: Int)

    override fun equals(other: Any?): Boolean {
        return if (other == null) false
        else other.javaClass == this.javaClass
    }

    override fun hashCode(): Int {
        return javaClass.hashCode()
    }

    internal fun internalBindViewHolder(holder: ViewHolder, item: Any, position: Int) {
        // Unchecked call, but it works magic
        bindViewHolder(holder as VH, item as V, position)
    }
}