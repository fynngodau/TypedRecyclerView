/*
 * CC0 – No rights reserved
 *
 * To the extent possible under law, Fynn Godau has waived all copyright and related
 * or neighboring rights to TypedRecyclerView. This work is published from Germany.
 */
package godau.fynn.typedrecyclerview

import android.content.Context
import android.view.LayoutInflater
import androidx.annotation.CallSuper
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder

/**
 * Recycler view implementation that stores context, content and inflater
 * for its child classes.
 *
 *  `<V>`: List value class
 *
 * `<VH>`: View holder class
 */
abstract class SimpleRecyclerViewAdapter<V, VH : ViewHolder>
@JvmOverloads constructor(@JvmField protected val content: MutableList<V> = mutableListOf()) :
    RecyclerView.Adapter<VH>() {
    /**
     * Available after adapter has been attached to recycler view
     */
    protected lateinit var context: Context

    /**
     * Available after adapter has been attached to recycler view
     */
    protected lateinit var inflater: LayoutInflater

    /**
     * Contains all recycler views that observe this adapter
     */
    protected var recyclerViews: MutableList<RecyclerView> = ArrayList()

    @CallSuper
    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        context = recyclerView.context
        inflater = LayoutInflater.from(context)
        recyclerViews.add(recyclerView)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        onBindViewHolder(holder, content[position], position)
    }

    abstract fun onBindViewHolder(holder: VH, item: V, position: Int)

    /**
     * @return 0 if content list is null, its size otherwise
     */
    override fun getItemCount(): Int {
        return content.size
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        recyclerViews.remove(recyclerView)
    }
}