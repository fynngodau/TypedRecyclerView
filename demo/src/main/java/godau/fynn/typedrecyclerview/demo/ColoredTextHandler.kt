package godau.fynn.typedrecyclerview.demo

import android.graphics.Color
import android.view.View.OnClickListener
import android.widget.TextView

class ColoredTextHandler(onClickListener: OnClickListener) : TextHandler(onClickListener) {
    override fun TextView.color() {
        setTextColor(Color.RED)
    }
}