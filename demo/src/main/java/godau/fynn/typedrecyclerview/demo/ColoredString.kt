package godau.fynn.typedrecyclerview.demo

import androidx.annotation.ColorRes

data class ColoredString(
    val string: String,
    @ColorRes val color: Int
) : CharSequence {
    override val length: Int
        get() = string.length

    override fun get(index: Int): Char = string[index]

    override fun subSequence(startIndex: Int, endIndex: Int): CharSequence = string.subSequence(startIndex, endIndex)
}