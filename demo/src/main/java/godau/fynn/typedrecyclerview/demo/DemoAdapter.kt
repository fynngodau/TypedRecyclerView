package godau.fynn.typedrecyclerview.demo

import godau.fynn.typedrecyclerview.TypeHandler
import godau.fynn.typedrecyclerview.TypedRecyclerViewAdapter

class DemoAdapter : TypedRecyclerViewAdapter<CharSequence>(
    (1..12).map { "Test string $it" }.toMutableList()
) {

    override fun getItemHandlerClass(
        item: CharSequence,
        position: Int
    ): Class<out TypeHandler<*, out CharSequence>>? {
        return if (position != 6) TextHandler::class.java else null
    }

    override fun getItemHandler(item: String, position: Int): TypeHandler<*, out String> {
        return ColoredTextHandler {
            content.add("An additional item")
            notifyItemInserted(content.size - 1)
        }
    }
}